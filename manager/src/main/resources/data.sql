-- Create Roles --
------------------
INSERT INTO Role (role_id,role) VALUES
(1, 'ADMIN'),
(2, 'CUSTOMER'),
(3, 'CUSTOMER_MANAGER'),
(4,'GROUP_MANAGER');


-- Init first user --
---------------------

INSERT INTO User (user_id,email, password, name,active) VALUES
  (5,'cmanager1', '123', 'cmanager1',1),
  (6,'admin', '123', 'admin1',1),
  (7,'customer1', '123', 'customer1',1),
  (8,'gmanager1', '123', 'gmanager1',1);

INSERT INTO USER_ROLE (user_id, role_id) VALUES
(5,3),
(6,1),
(7,2),
(8,4);

-- Init first classes --
------------------------

INSERT INTO Kurs (kurs_id, NAME) VALUES
(1,'YOGA'),
(2, 'Spinning'),
(3, 'Pilates'),
(4, 'Core');

-- Init first class run --
--------------------------

INSERT INTO Kursrun (kursrun_id, kurs_id) VALUES
(1,1),
(2,1),
(3,2),
(4,3),
(5,4);