package ch.highway2bit.repository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.highway2bit.entity.Kursrun;

@Repository
public interface KursrunRepository extends JpaRepository<Kursrun, Long> {
	@Query("select k from Kursrun k where k.date= ?1 AND k.startTime between ?2 and ?3")
	List<Kursrun> findNextClassRun(LocalDate date, LocalTime localTime, LocalTime localTime2);
}