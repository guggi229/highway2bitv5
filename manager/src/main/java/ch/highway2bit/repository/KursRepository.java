package ch.highway2bit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ch.highway2bit.entity.Kurs;


@Repository
public interface KursRepository extends JpaRepository<Kurs, Long> {

}