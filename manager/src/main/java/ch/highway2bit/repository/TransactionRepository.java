package ch.highway2bit.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ch.highway2bit.entity.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, String> {
	boolean existsById(String primaryKey);
}
