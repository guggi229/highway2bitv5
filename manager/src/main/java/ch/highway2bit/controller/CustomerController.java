package ch.highway2bit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import ch.highway2bit.model.alias.AliasConstans;
import ch.highway2bit.model.alias.AliasRegisterRequestModel;
import ch.highway2bit.service.UserService;

@Controller
@RequestMapping("/customer/")
public class CustomerController {

	// NAVIGATION
	public static final String NAV_TO_INDEX="customer/index";


	// EVENTS
	public static final String EVENT_INDEX="index";

	@Autowired
	private UserService userService;

	@GetMapping(EVENT_INDEX)
	public String index(Model model) {
		model.addAttribute("customers", userService.findAll());
		return NAV_TO_INDEX;
	}

}
