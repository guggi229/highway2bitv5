package ch.highway2bit.controller;

import java.io.Serializable;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import ch.highway2bit.exception.RefNoAlreadyExistException;
import ch.highway2bit.model.alias.AliasRegisterRequestModel;
import ch.highway2bit.model.alias.State;
import ch.highway2bit.service.AliasService;

@Controller
@RequestMapping("/alias/")
public class AliasController implements Serializable{
	private static final long serialVersionUID = -1598040780917571126L;
	private static final Logger LOG = LoggerFactory.getLogger(AliasController.class);
	// NAVIGATION
	public static final String NAV_TO_ALIAS_REG="alias/aliasreg";

	// EVENTS
	public static final String EVENT_ALIAS_REG="aliasreg";
	public static final String EVENT_ALIAS_REGISTRIERUNG="registrierung";

	@Autowired
	private AliasService aliasService;

	private AliasRegisterRequestModel aliasRegisterTransactionModel;


	@GetMapping(EVENT_ALIAS_REG)
	// public String aliasReg(@ModelAttribute("status") String state)
	//public String aliasReg(@RequestParam Map<String,String> allParams)
	public String  aliasReg(@RequestParam Map<String,String> allParams) {
		ModelAndView model = new ModelAndView();
//		if (allParams.get("status").equals(State.cancel.toString())){
//			LOG.info("User hat den Prozess abgebochen");
//		}
//		else if (allParams.get("status").equals(State.success.toString())){
//			LOG.info("Aliasregistrierung erfolgreich");
//		}


		return NAV_TO_ALIAS_REG;
	}

	@PostMapping(EVENT_ALIAS_REGISTRIERUNG)
	public RedirectView redirect(RedirectAttributes attributes){
	try {
		aliasRegisterTransactionModel = aliasService.generateAliasModel();
	} catch (RefNoAlreadyExistException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	attributes.addAttribute("merchantId", aliasRegisterTransactionModel.getMerchantId());
	attributes.addAttribute("amount", aliasRegisterTransactionModel.getAmount());
	attributes.addAttribute("currency", aliasRegisterTransactionModel.getCurrency());
	attributes.addAttribute("refno", aliasRegisterTransactionModel.getRefNo());
	attributes.addAttribute("sign", aliasRegisterTransactionModel.getSign());
	attributes.addAttribute("successUrl", aliasRegisterTransactionModel.getSuccessUrl());
	attributes.addAttribute("errorUrl", aliasRegisterTransactionModel.getErrorUrl());
	attributes.addAttribute("cancelUrl", aliasRegisterTransactionModel.getCancelUrl());
	attributes.addAttribute("uppWebResponseMethod",aliasRegisterTransactionModel.getUppWebResponseMethod());
	attributes.addAttribute("theme", aliasRegisterTransactionModel.getTheme());
	attributes.addAttribute("version", aliasRegisterTransactionModel.getVersion());
	attributes.addAttribute("uppReturnTarget", aliasRegisterTransactionModel.getUppReturnTarget());
	attributes.addAttribute("useAlias", aliasRegisterTransactionModel.getUseAlias());

	return new RedirectView(aliasService.getRequestUrl());
	}


}
