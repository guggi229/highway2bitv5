package ch.highway2bit.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import ch.highway2bit.entity.User;
import ch.highway2bit.repository.UserRepository;
import ch.highway2bit.service.UserService;

@Controller
@RequestMapping("/customermanager/")
public class CustomerManagingController {

	// NAVIGATION
	public static final String NAV_TO_INDEX="customermanager/index";
	public static final String NAV_TO_ADD="customermanager/add";

	// EVENTS
	public static final String EVENT_INDEX="index";
	public static final String EVENT_ADD="add";

	@Autowired
	private UserService userService;

	@GetMapping(EVENT_INDEX)
	public String index(Model model) {
		model.addAttribute("customers", userService.findAll());
		return NAV_TO_INDEX;
	}

	@GetMapping(EVENT_ADD)
	public String add(User model) {
		return NAV_TO_ADD;
	}

	@PostMapping(EVENT_ADD)
	public String addStudent(@Valid User user, BindingResult result, Model model) {
		userService.saveCustomer(user);
		return NAV_TO_INDEX;
	}
}
