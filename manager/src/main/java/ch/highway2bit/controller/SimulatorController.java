package ch.highway2bit.controller;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/simulator/")
public class SimulatorController {

	// NAVIGATION
	public static final String NAV_TO_INDEX="simulator/index";

	// EVENTS
	public static final String EVENT_INDEX="index";

	@GetMapping(EVENT_INDEX)
	public String index(BindingResult errors) {
		return NAV_TO_INDEX;
	}


}
