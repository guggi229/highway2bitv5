package ch.highway2bit.websocket;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.HtmlUtils;


@Controller
public class GreetingController {

	@MessageMapping("/hello")
   @SendTo("/topic/greetings")
    public Greeting greeting(HelloMessage message) throws Exception {
    	HelloMessage helloMessage = new HelloMessage("Test");
    	System.out.println("Sending");
        return new Greeting("Hello, " +helloMessage);
    }


}
