package ch.highway2bit.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

@Controller
public class ScheduledUpdatesOnTopic{
	   @Autowired
	    private SimpMessagingTemplate template;


	    public void greeting() throws InterruptedException {

	        System.out.println("scheduled");
	        this.template.convertAndSend("/topic/greetings", "Hello");
	    }
}
