package ch.highway2bit.service;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.highway2bit.exception.RefNoAlreadyExistException;
import ch.highway2bit.model.alias.AliasConstans;
import ch.highway2bit.model.alias.AliasRegisterRequestModel;
import ch.highway2bit.model.alias.UppReturnTarget;
import ch.highway2bit.model.global.Currency;
import ch.highway2bit.repository.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Service
public class AliasService {

	private AliasRegisterRequestModel aliasRegisterTransactionModel;
	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
	private static final Logger LOG = LoggerFactory.getLogger(AliasService.class);

	@Autowired
	private TransactionRepository transactionRepository;

	@Value("${datatrans.merchantId_3d_secure}")
	private String merchantId_3d;

	@Value("${datatrans.sign}")
	private Long sign;

	@Value("${datatrans.alias.create.url}")
	private String startAliasUrl;

	@Value("${datatrans.alias.success.url}")
	private String successUrl;

	@Value("${datatrans.alias.reg.error.url}")
	private String errorUrl;

	@Value("${datatrans.alias.reg.cancel.url}")
	private String cancelUrl;


	public AliasRegisterRequestModel generateAliasModel() throws RefNoAlreadyExistException{
		aliasRegisterTransactionModel = new AliasRegisterRequestModel();
		aliasRegisterTransactionModel.setMerchantId(merchantId_3d);
		aliasRegisterTransactionModel.setAmount(new BigDecimal("0"));
		aliasRegisterTransactionModel.setCurrency(Currency.CHF);
		aliasRegisterTransactionModel.setRefNo(generateUniqueRefNo());
		aliasRegisterTransactionModel.setSign(sign);
		aliasRegisterTransactionModel.setSuccessUrl(successUrl);
		aliasRegisterTransactionModel.setErrorUrl(errorUrl);
		aliasRegisterTransactionModel.setCancelUrl(cancelUrl);
		aliasRegisterTransactionModel.setUppWebResponseMethod(AliasConstans.UPP_WEB_RESPONSE_METHODE);
		aliasRegisterTransactionModel.setTheme(AliasConstans.THEME);
		aliasRegisterTransactionModel.setVersion(AliasConstans.VERSION);
		aliasRegisterTransactionModel.setUppReturnTarget(UppReturnTarget._top);
		aliasRegisterTransactionModel.setUseAlias(AliasConstans.USE_ALIAS);
		return aliasRegisterTransactionModel;
	}

	/**
	 * Generiert eine Unique Referenz Nummer, um die Transaction wieder zu finden.
	 * @return String
	 * @throws RefNoAlreadyExistException
	 */
	private String generateUniqueRefNo() throws RefNoAlreadyExistException{
		String refNo = LocalDateTime.now().format(formatter);
		if (transactionRepository.existsById(refNo)){
			LOG.error(MessageFormat.format("Referent {0} existiert bereits", refNo));
			throw new RefNoAlreadyExistException();
		}
		return refNo;
	}

	public String getRequestUrl(){
		return startAliasUrl;
	}



}
