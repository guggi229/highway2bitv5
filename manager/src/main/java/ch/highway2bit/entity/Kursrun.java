package ch.highway2bit.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({@NamedQuery(name = "Kursrun.findNextClassRun",
query = "select k from Kursrun k")
})
public class Kursrun implements Serializable {

	private static final long serialVersionUID = 6251969027619663610L;

	// PARAMS
	public static final String PARAM_GET_NEXT_KURS_RUN = "PARAM_GET_NEXT_KURS_RUN";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="kursrun_id")
	private Long id;

	@Column(name="DATE")
	private LocalDate date;

	@Column(name="TIME")
	private LocalTime startTime;

	@Column(name="AMOUNT_CUSTOMER")
	private Integer amountCustomer;

	@Column(name="DURATION")
	private Integer duration;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "kurs_id")
	private Kurs kurs;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public Integer getAmountCustomer() {
		return amountCustomer;
	}

	public void setAmountCustomer(Integer amountCustomer) {
		this.amountCustomer = amountCustomer;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Kurs getKurs() {
		return kurs;
	}

	public void setKurs(Kurs kurs) {
		this.kurs = kurs;
	}

}
