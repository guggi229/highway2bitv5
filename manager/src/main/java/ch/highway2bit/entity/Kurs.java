package ch.highway2bit.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Kurs implements Serializable{

	private static final long serialVersionUID = 4520078711015239138L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="kurs_id")
	private Long id;

	@Column(name="NAME")
	private String kursName;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKursName() {
		return kursName;
	}

	public void setKursName(String kursName) {
		this.kursName = kursName;
	}


}
