package ch.highway2bit.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;

import javax.persistence.Id;

import ch.highway2bit.model.global.Currency;
import ch.highway2bit.model.transaction.TransactionTyp;

@Entity
public class Transaction implements Serializable {

	private static final long serialVersionUID = 8554263603334836343L;

	@Id
	private String RefNo;
	private TransactionTyp transactionTyp;
	private BigDecimal amount;
	private Currency currency;

	public String getRefNo() {
		return RefNo;
	}
	public void setRefNo(String refNo) {
		RefNo = refNo;
	}
	public TransactionTyp getTransactionTyp() {
		return transactionTyp;
	}
	public void setTransactionTyp(TransactionTyp transactionTyp) {
		this.transactionTyp = transactionTyp;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public Currency getCurrency() {
		return currency;
	}
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}




}
