package ch.highway2bit.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Alias implements Serializable {
	private static final long serialVersionUID = 2602170320882312408L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ALIAS_ID")
	private Long id;

	@Column(name = "MERCHANT_ID")
	private Long merchantId;

	@Column(name = "AMOUNT")
	private BigDecimal amount;

	@Column(name = "REFERENZ_NUMBER")
	private Long refNumber;

	@Column(name = "SIGN")
	private Long sign;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(Long merchantId) {
		this.merchantId = merchantId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getRefNumber() {
		return refNumber;
	}

	public void setRefNumber(Long refNumber) {
		this.refNumber = refNumber;
	}

	public Long getSign() {
		return sign;
	}

	public void setSign(Long sign) {
		this.sign = sign;
	}


}
