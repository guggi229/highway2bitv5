package ch.highway2bit;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.Transactional;

import ch.highway2bit.entity.Role;
import ch.highway2bit.entity.User;
import ch.highway2bit.entity.Kurs;
import ch.highway2bit.entity.Kursrun;
import ch.highway2bit.repository.KursRepository;
import ch.highway2bit.repository.KursrunRepository;
import ch.highway2bit.repository.RoleRepository;
import ch.highway2bit.repository.UserRepository;
import ch.highway2bit.service.UserService;

@SpringBootApplication
@EnableScheduling
public class Application {
	private static final Logger log = LoggerFactory.getLogger(Application.class);


	@Autowired
	UserService userService;


	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	@Transactional
	public CommandLineRunner demo( UserRepository userRepository, RoleRepository roleRepository, KursRepository kursRepository, KursrunRepository kursrunRepository) {
		return (args) -> {
			User user1= userRepository.findByEmail("cmanager1");
			user1.setPassword("123");
			userService.save(user1);

			User user2 = userRepository.findByEmail("admin");
			user2.setPassword("123");
			userService.save(user2);

			User user3 = userRepository.findByEmail("customer1");
			user3.setPassword("123");
			userService.save(user3);

			User user4 = userRepository.findByEmail("gmanager1");
			user4.setPassword("123");
			user4 = userService.save(user4);

			int timePlus=0;
				for (Kursrun kursrun : kursrunRepository.findAll()) {
					kursrun.setStartTime(LocalTime.now().plusHours(timePlus));
					kursrun.setDuration(30);
					kursrun.setDate(LocalDate.now());
					++timePlus;
				kursrunRepository.save(kursrun);
			}



		};
	}
}
