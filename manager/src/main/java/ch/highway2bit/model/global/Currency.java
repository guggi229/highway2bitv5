package ch.highway2bit.model.global;


public enum Currency {
  CHF(756), EUR(978);

  private int m_code;

  private Currency(int code) {
    m_code = code;
  }

  public int getCode() {
    return m_code;
  }

  public static Currency fromCode(int code) {
    for (Currency w : Currency.values()) {
      if (w.getCode() == code) {
        return w;
      }
    }
    throw new IllegalArgumentException("Nicht gefunden: " + code + "'");
  }
}
