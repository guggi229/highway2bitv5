package ch.highway2bit.model.alias;


public final class AliasConstans {
	public static final String UPP_WEB_RESPONSE_METHODE="GET";
	public static final String THEME="DT2015";
	public static final String VERSION="2.0.0";
	public static final String USE_ALIAS="yes";
	public static final String UPP_REMEMBER_ME="checked";

    private AliasConstans(){

    }

}
