package ch.highway2bit.model.alias;

import java.io.Serializable;
import java.math.BigDecimal;

import ch.highway2bit.model.global.Currency;

public class AliasRegisterRequestModel implements Serializable{

	private static final long serialVersionUID = 1874074521567288955L;

	private String merchantId;

	private BigDecimal amount;

	private Currency currency;

	private String refNo;

	private Long sign;

	private String successUrl;

	private String errorUrl;

	private String cancelUrl;

	private String uppWebResponseMethod;

	private String theme;

	private String version;

	private UppReturnTarget uppReturnTarget;

	private String useAlias;

	private String uppRememberMe;


	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public Long getSign() {
		return sign;
	}

	public void setSign(Long sign) {
		this.sign = sign;
	}

	public String getSuccessUrl() {
		return successUrl;
	}

	public void setSuccessUrl(String successUrl) {
		this.successUrl = successUrl;
	}

	public String getErrorUrl() {
		return errorUrl;
	}

	public void setErrorUrl(String errorUrl) {
		this.errorUrl = errorUrl;
	}

	public String getCancelUrl() {
		return cancelUrl;
	}

	public void setCancelUrl(String cancelUrl) {
		this.cancelUrl = cancelUrl;
	}

	public String getUppWebResponseMethod() {
		return uppWebResponseMethod;
	}

	public void setUppWebResponseMethod(String uppWebResponseMethod) {
		this.uppWebResponseMethod = uppWebResponseMethod;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public UppReturnTarget getUppReturnTarget() {
		return uppReturnTarget;
	}

	public void setUppReturnTarget(UppReturnTarget uppReturnTarget) {
		this.uppReturnTarget = uppReturnTarget;
	}

	public String getUseAlias() {
		return useAlias;
	}

	public void setUseAlias(String useAlias) {
		this.useAlias = useAlias;
	}

	public String getUppRememberMe() {
		return uppRememberMe;
	}

	public void setUppRememberMe(String uppRememberMe) {
		this.uppRememberMe = uppRememberMe;
	}

	@Override
	public String toString() {
		return "AliasRegisterTransactionModel [merchantId=" + merchantId + ", amount=" + amount + ", currency="
				+ currency + ", refNo=" + refNo + ", sign=" + sign + ", successUrl=" + successUrl + ", errorUrl="
				+ errorUrl + ", cancelUrl=" + cancelUrl + ", uppWebResponseMethod=" + uppWebResponseMethod + ", theme="
				+ theme + ", version=" + version + ", uppReturnTarget=" + uppReturnTarget + ", useAlias=" + useAlias
				+ ", uppRememberMe=" + uppRememberMe + "]";
	}



}
