package ch.highway2bit.model.transaction;

public enum TransactionTyp {
	CREATE_ALIAS, PAY_WITH_ALIAS, PAY_WITHOUT_ALIAS
}
